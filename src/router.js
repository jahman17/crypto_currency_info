import VueRouter from 'vue-router';

import TransactionsComponent from './components/SecondPart/TransactionsComponent.vue';
import DesktopComponent from './components/FirstPart/DesktopComponent.vue';

const routes = [
  { path: '/first-part', component: DesktopComponent },
  { path: '/second-part', component: TransactionsComponent },
];

export default new VueRouter({
  routes,
});
