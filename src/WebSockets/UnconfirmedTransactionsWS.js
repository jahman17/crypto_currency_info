/**
 * Created by User on 18.11.2018.
 */
import WebSocketService from '../Services/WebSocketService'

export default class UnconfirmedTransactionsWS {
  isSubscribed = false;
  constructor(){
    let self = this;
    this.connection = new WebSocketService('wss://ws.blockchain.info/inv');
    this.connection.onOpen(function(){
      self.subscribe();
    });
  }

  subscribe(){
      this.connection.sendMessage({"op":"unconfirmed_sub"}); // take param from .env file
      this.isSubscribed = true;
  }

  unsubscribe(){
      this.connection.sendMessage({"op":"unconfirmed_unsub"}); // take param from .env file
      this.isSubscribed = false;
  }

  onMessage(callBack){
    return new Promise(() => {
      this.connection.onMessage(message => {
        callBack(this.assembleViewedData(JSON.parse(message.data)));
      });
    });
  }

  assembleViewedData(message){
    let from = [];
    let to = [];
    let sumOut = 0;
    message.x.inputs.forEach((input) => {
      from.push(input.prev_out.addr);
    });
    message.x.out.forEach((item) => {
      to.push(item.addr);
      sumOut += Number(item.value) / 100000000;
    });
    return {
      from: from,
      to: to,
      sumOut: sumOut,
    };
  }
}
