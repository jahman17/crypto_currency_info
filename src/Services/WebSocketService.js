/**
 * Created by User on 17.11.2018.
 */
export default class WebSocketService {
  constructor(url) {
    this.wsConnection = new WebSocket(url);
    // this.wsConnection.onopen = function (event) {
    //   console.log("WebSocket is open now:", event);
    // };
    // this.wsConnection.onmessage = function (event) {
    //   console.log("WebSocket message:", event);
    // };
    // this.wsConnection.onerror = function (event) {
    //   console.log("WebSocket error:", event);
    // };
    // this.wsConnection.onclose = function (event) {
    //   console.log("WebSocket close:", event);
    // };
  }

  sendMessage(message){
    this.wsConnection.send(JSON.stringify(message));
  }
  closeConnection(){
    this.wsConnection.close();
  }
  onMessage(callback) {
    this.wsConnection.onmessage = callback;
  }
  onOpen(callback) {
    this.wsConnection.onopen = callback;
  }


}