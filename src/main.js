import Vue from 'vue';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';
import VueDraggableResizable from 'vue-draggable-resizable';
import TransactionComponent from './components/SecondPart/TransactionComponent.vue';
import TransactionsComponent from './components/SecondPart/TransactionsComponent.vue';
import DesktopComponent from './components/FirstPart/DesktopComponent.vue';
import WindowComponent from './components/FirstPart/WindowComponent.vue';
import DeletedWindowsModal from './components/FirstPart/DeletedWindowsModalComponent.vue';
import router from './router';
import VueRouter from 'vue-router';
import Vuex from 'vuex'
import { store } from './stores/index';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.component('vue-draggable-resizable', VueDraggableResizable);
Vue.component('transactions-component', TransactionsComponent);
Vue.component('transaction-component', TransactionComponent);
Vue.component('desktop-component', DesktopComponent);
Vue.component('window-component', WindowComponent);
Vue.component('deleted-windows-modal', DeletedWindowsModal);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
