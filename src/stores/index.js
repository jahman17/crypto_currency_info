import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import WindowsModule from './modules/WindowsModule';
import DesktopModule from './modules/DesktopModule';
Vue.use(Vuex);

const vuePersist = {
  reducer: state => ({
    windows: state.windows,
    desktop: state.desktop,
  }),
  saveState: (key, state, storage) => {
    requestIdleCallback(() => {
      storage.setItem(key, JSON.stringify(state));
    });
  },
};

export const store = new Vuex.Store({
  strict: true,
  actions: {
    initAction (context) {
      context.commit('initStore');
    }
  },
  mutations: {
    initStore() {},
  },
  modules: {
    windows: WindowsModule,
    desktop: DesktopModule,
  },
  plugins: [createPersistedState(vuePersist)],
});