const state = {
  windowsData: {}
};

const getters = {
  getWindowData: state => data => {
    return state.windowsData[data] ? state.windowsData[data] : null;
  },
};

const actions = {
  /**
   *
   * @param context
   * @param data = {
   *  title: 'title 1',
   *  windowData: {WW: 300, WH: 100, WXP: 430, WYP: 270, isActive: false}
   * }
   */
  addWindowData(context, data){
    // console.log('in add');
    if (typeof data === 'object') {
      context.commit('addRecord', data);
    } else console.warn();
  },
  /**
   *
   * @param context
   * @param data = {
   *  title: 'title 1',
   *  windowData: {WW: 300, WH: 100, WXP: 430, WYP: 270, isActive: false}
   * }
   */
  updateWindowData(context, data){
    // console.log('in update');
    if (typeof data === 'object') {
      context.commit('rewriteRecord', data);
    } else console.warn();
  },
  /**
   *
   * @param context
   * @param data = string
   */
  deleteWindowData(context, data){
    if (typeof data === 'string') {
      context.commit('deleteRecord', data);
    } else console.warn();
  }
};

const mutations = {
  addRecord(state, data){
    state.windowsData[data.title] = data.windowData;
  },
  rewriteRecord(state, data){
    state.windowsData[data.title] = data.windowData;
  },
  deleteRecord(state, data){
    delete state.windowsData[data]
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};