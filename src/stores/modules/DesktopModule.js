const state = {
  deletedWindows: [],
  restoredWindowIndex: 0,
  windows: [
    {title: 'Title 1', isActive: false},
    {title: 'Title 2', isActive: false},
    {title: 'Title 3', isActive: false},
    {title: 'Title 4', isActive: false},
    {title: 'Title 5', isActive: false},
  ],
};

const getters = {
  getWindows: state => {
    return state.windows;
  },
  getRestoredWindow: state => {
    return state.windows[state.restoredWindowIndex];
  },
  getDeletedWindows: state => {
    return state.deletedWindows;
  }
};

const actions = {
  addWindow(context, data){
    if (typeof data === 'object') {
      context.commit('addRecord', data);
    } else console.warn('Wrong type! Given' + typeof data + '. Must be object');
  },
  addWindows(context, data){
    if (Array.isArray(data)) {
      context.commit('addRecords', data)
    } else console.warn();
  },
  deleteWindow(context, data){
    if (typeof data === 'string') {
      context.commit('deleteRecord', data)
    } else console.warn();
  },
  restoreWindow(context, data){
    if (typeof data === 'number') {
      context.commit('restoreRecord', data);
    } else console.warn('Wrong type! Given' + typeof data + '. Must be number');
  },
  activateWindow(context, data){
    if (typeof data === 'string') {
      context.commit('activateRecord', data)
    } else console.warn();
  }
};

const mutations = {
  addRecord(state, data) {
    state.windows.push(data);
  },
  addRecords(state, data) {
    state.windows = data;
  },
  deleteRecord(state, data) {
    let deletedWindowIndex = 0;
    let deletedWindow = state.windows.filter((item, key) => {
      if (item.title === data){
        deletedWindowIndex = key;
        return true;
      }
      return false;
    });
    state.deletedWindows.push(deletedWindow[0]);
    state.windows.splice(deletedWindowIndex, 1);
  },
  restoreRecord(state, data){
    state.restoredWindowIndex = state.windows.push(state.deletedWindows[data]) - 1;
    state.deletedWindows.splice(data, 1);
  },
  activateRecord(state, data) {
    state.windows.forEach((item, key) => {
      state.windows[key].isActive = item.title === data;
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};